# Install Veilid package source
wget -O- https://packages.veilid.net/gpg/veilid-packages-key.public | gpg --dearmor > /usr/share/keyrings/veilid-packages-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/veilid-packages-keyring.gpg] https://packages.veilid.net/apt stable main" > /etc/apt/sources.list.d/veilid.list

# Install Veilid server (and start it) and CLI and support for Python 3.11 installation
apt-get update
apt install -y software-properties-common veilid-server veilid-cli
systemctl enable --now veilid-server.service

# Install Python3.11
add-apt-repository -y ppa:deadsnakes/ppa
apt-get install -y python3.11

# Download the Veilid Python demo; download poetry; set up poetry
runuser -l vagrant -c 'curl -sSL https://install.python-poetry.org | python3 - && git clone https://gitlab.com/veilid/python-demo.git'

# Indicate remaining steps
cat <<'EOF'
# To test the Veilid Python Chat demo do the following:
# (visit https://gitlab.com/veilid/python-demo for more details)
# F1> is Friend1's computer, F2> is Friend2's computer

F1> export PATH="/home/vagrant/.local/bin:$PATH"
F1> cd ~/python-demo
F1> poetry install

F2> export PATH="/home/vagrant/.local/bin:$PATH"
F2> cd ~/python-demo
F2> poetry install

F1> poetry run chat keygen
# Public key referred to as $F1
# Private key left in .demokeys
F2> poetry run chat keygen
# Public key referred to as $F2
F1> poetry run chat add-friend Friend2 $F2
F1> poetry run chat start Friend2
# Chat key into $CK
F2> poetry run chat add-friend Friend1 $F1
F2> poetry run chat respond Friend1 $CK
EOF


